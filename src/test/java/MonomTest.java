import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by teo on 14/03/2017.
 */
public class MonomTest {
    @Test
    public void toStringM()
    {
        Monom m = new Monom(-5,3);
        assertEquals("-5.0x^3", m.toStringM());
    }

    @Test
    public void addM()
    {
        Monom m1 = new Monom(5, 3);
        Monom m2 = new Monom(4, 3);
        assertEquals("+9.0x^3", Monom.addM(m1,m2).toStringM());
    }

    @Test
    public void subM()
    {
        Monom m1 = new Monom( -4, 6);
        Monom m2 = new Monom(-3, 6);
        assertEquals("-1.0x^6", Monom.subM(m1, m2).toStringM());

    }

    @Test
    public void inmM()
    {
        Monom m1 = new Monom(3, 4);
        Monom m2 = new Monom( 2, 4);
        assertEquals("+6.0x^8", Monom.inmM(m1, m2).toStringM());

    }

    @Test
    public void impM()
    {
        Monom m1 = new Monom(6, 3);
        Monom m2 = new Monom(2, 1);
        assertEquals("+3.0x^2", Monom.impM(m1, m2).toStringM());
    }

    @Test
    public void deriv()
    {
        Monom m = new Monom(4, 2);
        assertEquals("+8.0x^1", Monom.deriv(m).toStringM());
    }

    @Test
    public void integr()
    {
        Monom m = new Monom(4, 2);
        assertEquals("+1.3333333333333333x^3", Monom.integr(m).toStringM());
    }

    @Test
    public void negare()
    {
        Monom m = new Monom(-5, 5);
        assertEquals("+5.0x^5", Monom.negare(m).toStringM());
    }

}