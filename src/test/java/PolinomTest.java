
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by teo on 14/03/2017.
 */
public class PolinomTest {
    @Before
    public void setUp() throws Exception {
        System.out.println("Aici incepe un test nou");

    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Aici se termina testul");
    }

    @Test
    public void testToString()
    {
        Polinom p = new Polinom("5x^3 - 9x^2 + 1");
        assertEquals("+5.0x^3-9.0x^2+1.0x^0", p.toString());
    }

    @Test
    public void TestAddP()
    {
        Polinom p1 = new Polinom("2x^1 + 4" );
        Polinom p2 = new Polinom( " 5x^1 + 3");
        assertEquals("+7.0x^1+7.0x^0", Polinom.addP(p1,p2).toString());
    }

    @Test
    public void TestAddP1()
    {
        Polinom p1 = new Polinom("2x^10 + 4x^4 - 5x^3 + 2" );
        Polinom p2 = new Polinom( " 5x^1 + 3");
        assertEquals("+2.0x^10+4.0x^4-5.0x^3+5.0x^0+5.0x^1", Polinom.addP(p1,p2).toString());
    }

    @Test
    public void TestAddP2()
    {
        Polinom p2 = new Polinom("2x^10 + 4x^4 - 5x^3 + 2" );
        Polinom p1 = new Polinom( " 5x^1 + 3");
        assertEquals("+2.0x^10+4.0x^4-5.0x^3+5.0x^0+5.0x^1", Polinom.addP(p2,p1).toString());
    }

    @Test
    public void TestSubP()
    {
        Polinom p1 = new Polinom("2x^3 - 5x^1");
        Polinom p2 = new Polinom("1x^3 - 2x^1");
        assertEquals("+1.0x^3-3.0x^1", Polinom.subP(p1, p2).toString());

    }

    @Test
    public void TestSubP1()
    {
        Polinom p1 = new Polinom("3x^8 + 2x^3 - 5x^1  + 9x^0");
        Polinom p2 = new Polinom("1x^3 - 2x^1");
        assertEquals("+3.0x^8+1.0x^3-3.0x^1+9.0x^0", Polinom.subP(p1, p2).toString());
    }

    @Test
    public void TestSubP2()
    {
        Polinom p2 = new Polinom("3x^8 + 2x^3 - 5x^1  + 9x^0");
        Polinom p1 = new Polinom("1x^3 - 2x^1");
        assertEquals("+3.0x^8+1.0x^3-3.0x^1+9.0x^0", Polinom.subP(p2, p1).toString());
    }

    @Test
    public void inmP() throws Exception {
        Polinom p1 = new  Polinom("1x^1 + 1x^0");
        Polinom p2 = new Polinom("1x^1 + 1x^0");
        assertEquals("+1.0x^2+2.0x^1+1.0x^0", Polinom.inmP(p1,p2).toString());
    }

    @Test
    public void inmP1() throws Exception {
        Polinom p1 = new  Polinom("4x^3 +1x^1 + 1x^0");
        Polinom p2 = new Polinom("1x^1 + 1x^0");
        assertEquals("+4.0x^4+4.0x^3+1.0x^2+2.0x^1+1.0x^0", Polinom.inmP(p1,p2).toString());
    }

    @Test
    public void inmP2() throws Exception {
        Polinom p2 = new  Polinom("4x^3 +1x^1 + 1x^0");
        Polinom p1 = new Polinom("1x^1 + 1x^0");
        assertEquals("+4.0x^4+4.0x^3+1.0x^2+2.0x^1+1.0x^0", Polinom.inmP(p2,p1).toString());
    }

    @Test
    public void intP() throws Exception {
        Polinom p = new Polinom("2x^1+ 3");
        assertEquals("+1.0x^2+3.0x^1+1.0x^0", Polinom.intP(p).toString());
    }

    @Test
    public void derivP() throws Exception {
        Polinom p = new Polinom("2x^2+ 3");
        assertEquals("+4.0x^1", Polinom.derivP(p).toString());

    }

    @Test
    public void impP() throws Exception {
        Polinom p1 = new Polinom("2x^5 + 3x^3 + 4x^1");
        Polinom p2 = new Polinom("1x^2");
        Polinom [] p = Polinom.impP(p1, p2);
        assertEquals("+2.0x^3+3.0x^1", p[0].toString());
        assertEquals("+4.0x^1", p[1].toString());
    }

}
