public class Monom {
    private double coef;
    private int putere;
    // Acest constructor are rolul de usura creare polinoamelor.
    // Polinoamele folosite in operatii sunt create cu ajutoarul monoamelor.
    // Clasa monom are doua atribute : coeficient si putere.
    public Monom( double coef, int putere )
    {
        this.coef = coef;
        this.putere = putere;
    }

    public Monom ( Monom m )
    {
        coef = m.getCoef();
        putere = m.getPutere();
    }

    public int getPutere ()
    {
        return  putere;
    }

    public double getCoef ()
    {
        return  coef;
    }
    // Metoda toString face dintr-un monom un String, care va fi afisat.
    // Daca coeficientul monomului este mai mare decat zero atunci in String i se va adauga un plus, in caz contrar acesta contine deja semnul sau
    public String toStringM ( )
    {
        String s = "";
        if (coef > 0)
            s += "+";
        s += coef + "x^" + putere;
        return s;
    }
    // Metode add verifica daca puterile celor doua monoame sunt egale si daca conditia este adevarata aduna coeficientii.
    // Aceasta metoda este utila cand incercam sa adunam un monom la un polinom.
    public void add ( Monom m )
    {
        if ( m.getPutere() == putere)
            this.coef += m.coef;
    }
    // Metoda addM este aproape la fel ca cea de sus, dar aceasta returneaza un monom nou, care contine suma celor 2 monomi primiti ca si parametrii
    public static Monom addM ( Monom m1, Monom m2)
    {
        if( m1.getPutere() == m2.getPutere())
            return new Monom(m1.getCoef() + m2.getCoef(), m2.getPutere());
        return null;
    }
    // subM returneaza un monom nou, care contine rezultatul scaderii dintre m1 si m2.
    public static Monom subM ( Monom m1, Monom m2 )
    {
        if( m1.getPutere() == m2.getPutere())
            return new Monom(m1.getCoef() - m2.getCoef(), m2.getPutere());
        return null;
    }
    // Metoda inmM returneaza produsul dintre cei doi monomi primiti ca si parametrii.
    public static Monom inmM ( Monom m1, Monom m2)
    {
        return new Monom( m1.getCoef()*m2.getCoef(), m1.getPutere() + m2.getPutere());
    }
    // Metoda ipmM returneaza catul impartirii dintre cei doi monomi primiti ca si parametrii.
    public static Monom impM ( Monom m1, Monom m2)
    {
        return new Monom( m1.getCoef()/m2.getCoef(), m1.getPutere() - m2.getPutere());
    }
    // Aceasta metoda returneaza intr-un nou monom monomul primit ca parametru derivat .
    public static Monom deriv( Monom m)
    {

        return new Monom(m.getCoef()*m.getPutere(), m.getPutere()-1);
    }
    // Aceasta metoda returneaza intr-un nou monom monomul primit ca parametru integrat.
    public static Monom integr ( Monom m)
    {
        return new Monom( m.getCoef() / (m.getPutere() + 1), m.getPutere() +1);
    }
    // Metoda negare schimba semnul coeficientului monomului primt ca parametru
    public static Monom negare(Monom m){    return new Monom(-m.getCoef(), m.getPutere());}
}
