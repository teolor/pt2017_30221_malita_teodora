import javax.swing.*;

/**
 * Created by Teo on 3/7/2017.
 */
public class Main
{
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        new GUI("Operatii pe polinoame");
                    }
                }
        );
    }
}
