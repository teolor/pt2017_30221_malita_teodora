import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class GUI extends JFrame {
    private JPanel mainPanel;
    private SpringLayout layout = new SpringLayout();
    private static final int W = 1000;
    private static final int H = 625;
    private JTextField pol1 = new JTextField("Polinom 1:", 43);
    private JTextField pol2 = new JTextField("Polinom 2 :", 43);
    private JTextField out = new JTextField("Rezultat :", 45);
    private JTextField rest = new JTextField("Rest :", 45);
    private JButton derB;

    public GUI(String titlu)
    {
        setSize(W, H);
        setTitle(titlu);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        createMainPanel();
        setVisible(true);
        add(mainPanel);
    }
    // se creaza fundalul si se seteaza SpringLayout-ul
    // se apeleaza metodele de creare a bunoanelor si a textField-urilor
    private void createMainPanel()
    {
        mainPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                try {
                    BufferedImage img = ImageIO.read(getClass().getResource("b.jpg"));
                    g.drawImage(img, 0, 0, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
        mainPanel.setLayout(layout);
        creareButonAdd();
        creareButonSub();
        creareButonInm();
        creareButonImp();
        creareButonDer();
        creareButonInt();
        creareTextField1();
        creareTextField2();
        creareTextField3();
        creareTextField4();
    }
    // se creaza butoanele, la care se setam pozitia, culoarea si actiune pe care sa o face
    private void creareButonAdd()
    {
        JButton addB = new JButton("Adunare de polinoame");
        layout.putConstraint(SpringLayout.NORTH, addB, 10, SpringLayout.NORTH, mainPanel);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, addB, 0, SpringLayout.HORIZONTAL_CENTER, mainPanel);
        mainPanel.add(addB);
        addB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                out.setText(Polinom.addP(new Polinom(pol1.getText()), new Polinom(pol2.getText())).toString());
            }
        });
        addB.setBackground(Color.cyan);
        addB.setForeground(Color.black);
    }

    private void creareButonSub()
    {
        JButton subB = new JButton("Scadere de polinoame");
        layout.putConstraint(SpringLayout.NORTH, subB, 60, SpringLayout.NORTH, mainPanel);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, subB, 0, SpringLayout.HORIZONTAL_CENTER, mainPanel);
        mainPanel.add(subB);
        subB.setBackground(Color.cyan);
        subB.setForeground(Color.black);
        subB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                out.setText(Polinom.subP(new Polinom(pol1.getText()), new Polinom(pol2.getText())).toString());
            }
        });
    }

    private void creareButonInm()
    {
        JButton inmB = new JButton("Inmultire de polinoame");
        layout.putConstraint(SpringLayout.NORTH, inmB, 110, SpringLayout.NORTH, mainPanel);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, inmB, 0, SpringLayout.HORIZONTAL_CENTER, mainPanel);
        mainPanel.add(inmB);
        inmB.setBackground(Color.cyan);
        inmB.setForeground(Color.black);
        inmB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                out.setText(Polinom.inmP(new Polinom(pol1.getText()), new Polinom(pol2.getText())).toString());
            }
        });

    }

    private void creareButonImp() {
        JButton impB = new JButton("Impartire de polinoame");
        layout.putConstraint(SpringLayout.NORTH, impB, 160, SpringLayout.NORTH, mainPanel);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, impB, 0, SpringLayout.HORIZONTAL_CENTER, mainPanel);
        mainPanel.add(impB);
        impB.setBackground(Color.cyan);
        impB.setForeground(Color.black);
        impB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Polinom[] array = Polinom.impP(new Polinom(pol1.getText()), new Polinom(pol2.getText()));
                out.setText(array[0].toString());
                rest.setText(array[1].toString());
            }
        });

    }

    private void creareButonInt()
    {
        JButton intB = new JButton("Integrare de polinoame");
        layout.putConstraint(SpringLayout.NORTH, intB, 210, SpringLayout.NORTH, mainPanel);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, intB, 0, SpringLayout.HORIZONTAL_CENTER, mainPanel);
        mainPanel.add(intB);
        intB.setBackground(Color.cyan);
        intB.setForeground(Color.black);
        intB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                out.setText(Polinom.intP(new Polinom(pol1.getText())).toString());
            }
        });
    }

    private void creareButonDer()
    {
        derB = new JButton("Derivare de polinoame");
        layout.putConstraint(SpringLayout.NORTH, derB, 260, SpringLayout.NORTH, mainPanel);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, derB, 0, SpringLayout.HORIZONTAL_CENTER, mainPanel);
        mainPanel.add(derB);
        derB.setBackground(Color.cyan);
        derB.setForeground(Color.black);
        derB.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                out.setText(Polinom.derivP(new Polinom(pol1.getText())).toString());
            }
        });
    }
    // se creaza TextField-urile si se seteaza ca in momentul in care dam click pe ele sa se stearga continutul
    private void creareTextField1()
    {
        layout.putConstraint(SpringLayout.WEST, pol1, 10, SpringLayout.WEST, mainPanel);
        layout.putConstraint(SpringLayout.NORTH, pol1, 10, SpringLayout.SOUTH, derB);
        mainPanel.add(pol1);
        pol1.setBackground(new Color(192, 255, 243));
        pol1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                pol1.setText("");
            }
        });
    }

    private void creareTextField2()
    {
        layout.putConstraint(SpringLayout.WEST, pol2, 10, SpringLayout.EAST, pol1);
        layout.putConstraint(SpringLayout.NORTH, pol2, 10, SpringLayout.SOUTH, derB);
        mainPanel.add(pol2);
        pol2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                pol2.setText("");
            }
        });
        pol2.setBackground(new Color(192, 255, 243));
    }

    private void creareTextField3()
    {
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, out, 0, SpringLayout.HORIZONTAL_CENTER, mainPanel);
        layout.putConstraint(SpringLayout.NORTH, out, 50, SpringLayout.SOUTH, derB);
        mainPanel.add(out);
        out.setBackground(new Color(192, 255, 243));
    }

    private void creareTextField4()
    {
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, rest, 0, SpringLayout.HORIZONTAL_CENTER, mainPanel);
        layout.putConstraint(SpringLayout.NORTH, rest, 100, SpringLayout.SOUTH, derB);
        mainPanel.add(rest);
        rest.setBackground(new Color(192, 255, 243));

    }
}
