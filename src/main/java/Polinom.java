import java.util.ArrayList;
import java.util.*;

public class Polinom
{

    private ArrayList<Monom> p ;
    //
    public Polinom ( String s )
    {
        if (s != null) {
            p = new ArrayList<Monom>();
            s = s.replaceAll("\\s", "");
            s = s.replaceAll("-", "+-");

            String[] a = s.split("\\+");
            for (String ss : a) {
                String[] sss = ss.split("x");
                if (sss.length == 1) {
                    p.add(new Monom(Integer.parseInt(sss[0]), 0));
                } else {
                    sss[1] = sss[1].replaceAll("\\^", "");
                    p.add(new Monom(Integer.parseInt(sss[0]), Integer.parseInt(sss[1])));
                }
            }
        }
        else
            p = new ArrayList<Monom>();

    }
    private ArrayList<Monom> getMonoame ()
    {
        return p;
    }
    // Metoda toString din aceasta clasa, parcurge ArrayList-ul de Monoame si pentru fiecare dintre ele apeleaza metoda toString aflata in clasa Monom
    public String toString ()
    {
        String s  = "";
        for ( Monom m : p )
            s+= m.toStringM();
        return s;
    }
    // Metode copy copiaza un un polinom intr-un alt polinom, folosind metoda addM.
    // O vom folosi la impartirea de polinoame, cand in rest vom copia deimpartitul
    private static Polinom copy ( Polinom pol )
    {
        Polinom p = new Polinom(null);
        for ( Monom m : pol.getMonoame() )
            p.addM(new Monom(m));
        return p;
    }
    // addM parcurge Array List-ul si daca puterea monomului primit ca si parametru este egala cu a unuia din termenii din ArrayList atunci aceasta metoda va aduna monomul la monomul gasit cu aceeasi putere din ArrayList
    private void addM( Monom m )
    {
        for ( Monom m1 : p )
            if ( m1.getPutere() == m.getPutere()) {
                m1.add(m);
                return;
            }
        p.add(m);
    }
    // addP aduna cele 2 polinoame primite ca parametru
    // Returneaza intr-un nou polinom.(p3)
    //Prima data se aduna la p3( care este intializat cu null) polinomul p1(primit ca paratemtru), dupa care se aduna si polinomul p2(primit ca parametru)
    // Verifica daca coeficientul este zero si daca este il sterge din ArrayList.
    public static Polinom  addP ( Polinom p1, Polinom p2 )
    {
        Polinom p3 = new Polinom(null);
        for ( Monom m : p1.getMonoame())
            p3.addM( m);
        for ( Monom m : p2.getMonoame())
            p3.addM( m);
        for (int i = 0; i < p3.getMonoame().size(); ++i){
            if (p3.getMonoame().get(i).getCoef() == 0)
                p3.getMonoame().remove(i);
        }
        return p3;
    }
    // subP scade cele 2 polinoame primite ca parametru
    // Returneaza intr-un nou polinom.(p3)
    //Prima data se aduna la p3( care este intializat cu null) polinomul p1(primit ca paratemtru), dupa care se aduna la p3 si polinomul p2 negat(folosiind metoda negare din clasa Monom).
    // Verifica daca coeficientul este zero si daca este il sterge din ArrayList.
    public static Polinom  subP ( Polinom p1, Polinom p2 )
    {
        Polinom p3 = new Polinom(null);
        for ( Monom m : p1.getMonoame())
            p3.addM( m);
        for ( Monom m : p2.getMonoame())
            p3.addM( Monom.negare(m));
        for (int i = 0; i < p3.getMonoame().size(); ++i){
            if (p3.getMonoame().get(i).getCoef() == 0)
                p3.getMonoame().remove(i);
        }
        return p3;
    }
    // Metoda inmP parcurege cele doua ArrayList-uri de polinoame si pentru fiecare din monoame apeleaza metoda inmM din clasa Monom
    // Returneaza in Polinomul p3, initializat cu null, in rezultatele sunt adaugate utilizand metoda addM.
    public static Polinom  inmP ( Polinom p1, Polinom p2 )
    {
        Polinom p3 = new Polinom(null);
        for ( Monom m1 : p1.getMonoame())
            for ( Monom m2 : p2.getMonoame())
                p3.addM(Monom.inmM(m1, m2));
        return p3;
    }
    // Parcurge ArrayList-ul de polinoame si pentru fiecare monom din el apeleaza motoda inrgr din clasa Monom
    // Pentru a avea si constanta la ArrayList-ul rezultat si se adaga si un monom cu coeficientul 1 si de putere 0
    public static Polinom intP( Polinom p )
    {
        Polinom ip = new Polinom(null);
        for ( Monom m : p.getMonoame() )
        {
            ip.addM(Monom.integr(m));
        }
        ip.addM(new Monom(1, 0));
        return ip;
    }
    // Parcurge ArrayList-ul de polinoame si pentru fiecare monom din el apeleaza motoda deriv din clasa Monom
    // Daca puterea este negativa atunci nu se adauga la polinomul rezultat
    public static Polinom derivP( Polinom p )
    {
        Polinom id = new Polinom(null);
        for ( Monom m : p.getMonoame() )
        {
            Monom md = Monom.deriv(m);
            if ( md.getPutere() < 0 )
                continue;
            id.addM(md);
        }
        return id;
    }
    // Verifica daca un ArrayList-ul de monoame este gol.
    private boolean isNull()
    {
        if( p ==null)
            return true;
        return false;
    }
    // Returneaza gradul maxim din ArrayList
    private int grad()
    {
        int max = -1;
        for (Monom m: p)
        {
            if( m.getPutere() > max )
                max = m.getPutere();
        }
        return max;
    }
    // Returneaza monomul de putere maxima
    private  Monom maxM()
    {
        Monom max = new Monom(0, -1);
        for ( Monom m : p)
        {
            if ( m == null || m.getPutere() > max.getPutere() )
                max = m;
        }
        return max;
    }
    // Returneaza produsul dintr-un monom si un polinom.
    private static Polinom inmPM ( Polinom p, Monom m )
    {
        Polinom p1 = new Polinom(null);
        for ( Monom mm : p.getMonoame())
        {
            p1.addM( Monom.inmM(m, mm));
        }
        return p1;
    }
    // aici se face impartirea
    // utilizand metodele grad(), maxM() si copy
    public static Polinom[] impP ( Polinom d, Polinom i)
    {
        if ( i.grad() < 0 )
            return null;
        Polinom c = new Polinom(null);
        Polinom r = Polinom.copy(d);

        while ( r.grad() > 0 && r.grad() >= i.grad())
        {
            Monom t = Monom.impM(r.maxM(), i.maxM());
            c.addM(t);
            r = Polinom.subP(r, Polinom.inmPM(i, t));
        }

        return new Polinom[]{c, r};
    }
}
